# Printable resume

## Translation

```bash
# Extract texts to translate
pybabel extract --mapping babel.cfg --output-file=src/locale/resume.pot .
# Init new language or update existing languages with new translations
pybabel init -i src/locale/resume.pot -d src/locale -D resume -l fr
pybabel update -d src/locale -D resume -i src/locale/resume.pot
# Manually filling translations
pybabel compile -d src/locale/ -D resume --statistics
```
