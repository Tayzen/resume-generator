import yaml
from jinja2 import Environment
from i18n import TranslationsHandler
import os

# TODO: paths module to compute paths
# TODO: create a template handler
# TODO: create a content handler
# TODO: example template

if __name__ == "__main__":
    jinja_env = Environment(extensions=["jinja2.ext.i18n"])

    base_folder = "/".join(os.path.dirname(__file__).split("/")[:-1])
    template_name = "purple_blue"
    template_folder = os.path.join(base_folder, "templates", template_name)

    localedir = os.path.join(template_folder, "locale")

    translations = TranslationsHandler("resume", locale_dir=localedir)
    translations.locale = "en"
    jinja_env.install_gettext_translations(translations.translation)
    translations.locale = "fr"
    jinja_env.install_gettext_translations(translations.translation)

    with open(
        os.path.join(base_folder, "contents", f"content_{translations.locale}.yml"), "r"
    ) as fp:
        content = yaml.safe_load(fp)

    with open(os.path.join(template_folder, "index.html"), "r") as fp:
        text_template = fp.read()
        template = jinja_env.from_string(text_template)

    result = template.render(content)
    # TODO: minify the result

    print(result)

    with open("out/resume_fr.html", "w") as fp:
        fp.write(result)
