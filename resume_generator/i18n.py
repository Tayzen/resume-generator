import os
import gettext


class NotAnAvailableTranslation(Exception):
    pass


class TranslationsHandler:
    def __init__(self, domain, locale_dir, fallback_langs=["en"]) -> None:
        self.domain = domain
        self.locale_dir = locale_dir

        self.fallback_langs = []
        self.locales = []
        self.translations = {}
        self._locale = None

        self._refresh_translations(fallback_langs)
        self.translation = self.translations[self.locale]

    @property
    def locale(self):
        if self._locale is not None:
            return self._locale
        else:
            return self.fallback_langs[0] if self.fallback_langs else self.locales[0]

    @locale.setter
    def locale(self, value):
        if value in self.locales:
            self._locale = value
            self.translation = self.translations[self.locale]
        else:
            raise self._raise_unavailable_langs_error([value])

    # @property
    # def translation(self):
    #     return self.translations[self.locale]

    def _raise_unavailable_langs_error(self, langs):
        message = f"These languages are not available: {langs}. The available languages are: {self.locales}"
        raise NotAnAvailableTranslation(message)

    def _refresh_translations(self, fallback_langs):
        self._extract_locales()
        self._load_default_langs(fallback_langs)
        self._generate_translations()

    def _extract_locales(self):
        for _, dirnames, _ in os.walk(self.locale_dir):
            # TODO: self.locales = dirnames
            for dirname in dirnames:
                print(f"dir: {dirnames}")
                self.locales.append(dirname)
            break

    def _generate_translations(self):
        for lang in self.locales:
            self.translations[lang] = gettext.translation(
                self.domain,
                self.locale_dir,
                [lang] + self.fallback_langs,
            )

    def _load_default_langs(self, fallback_langs):
        unavailable_langs = []
        for lang in fallback_langs:
            if lang not in self.locales:
                unavailable_langs.append(lang)
        if not unavailable_langs:
            self.fallback_langs = fallback_langs
        else:
            raise self._raise_unavailable_langs_error(unavailable_langs)
